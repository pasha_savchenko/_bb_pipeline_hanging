from unittest.mock import MagicMock


def test_mock():
    assert MagicMock()


def test_mock_inf_recursion():
    obj = MagicMock()
    parent = obj.parent
    while parent:
        parent = parent.parent
